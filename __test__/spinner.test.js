import expect from 'expect';
import React from 'react';
//import TestUtils from 'react-addons-test-utils';

import { shallow, mount } from 'enzyme'
import Spinner from '../src/js/app/components/spinner';

describe('<Spinner />', () => {
    it('should be a div with a spinner class', () => {
        const props = {color : 'red'}
        const wrapper = shallow(<Spinner bgColor={props.color}/>)
        expect(wrapper.is('div.spinner')).toBe(true);
    })
});

/*
describe('Our first test', () => {
  test('two plus two is four', () => {
  expect(2 + 2).toBe(4);
  });
});
*/