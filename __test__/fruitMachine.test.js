import React from 'react'
import { shallow, mount } from 'enzyme'

import FruitMachine from '../src/js/app/components/fruitMachine';

describe('<FruitMachine />', () => {

    it('should be a div with a fruiMachine class', () => {
        const wrapper = shallow(<FruitMachine />);
        expect(wrapper.is('div.wrapper')).toBe(true);
    });

    it('should have 3 spiner-boxes components', () => {
        const wrapper = shallow(<FruitMachine />);
        expect(wrapper.find('Spinner').length).toEqual(3);
    });

    it('should have a play button', () => {
        const wrapper = shallow(<FruitMachine />);
        expect(wrapper.find('button').exists()).toBe(true);
    });

});