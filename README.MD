# When You Move Coding Test

## We are looking to determine how you approach creating a coded solution from scratch. Please treat this as if you are creating production ready code. We expect this to reflect how you would deliver to a client, so take that into account when creating your solution.

The aim of this short exercise is to simulate a fruit machine game in React. A fruit machine consists of three wheels, each wheel containing four colours

* Red
* Blue
* Green
* Yellow

The player spins the wheels by clicking on a button labelled "PLAY".  On playing the game, each wheel should use a random number to decide which colour to land on and the results of the three wheels should be displayed to the user.  If all three wheels land on the same colour a message should be displayed declaring that you have won the game.

You do not need to do any styling if you don't want to, three boxes with the results of each wheel, plus a "WINNER" message will suffice.

# Building the code

Install packages and build the code using

`> npm install`

`> npm run build`

From then on you can use [webpack-dasboard](https://github.com/FormidableLabs/webpack-dashboard) to watchyour files and run the node.js server

`> npm start`

You should then be able to navigate to [http://localhost:3000](http://localhost:3000) in your browser

There is one react component created for you in [src/js/app/index.js](https://bitbucket.org/whenyoumove/reactcodingtest/src/8a05ec9a9e086e75a50f9bd5052d50b342ae847f/src/js/app/?at=master)

Once complete, make a pull request from your fork to allow us to review your solution.