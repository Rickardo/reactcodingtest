/**
 * Allows Jest to ignore files set up in the configuration (package.json)
 * e.g. in this case .png files.
 */
module.exports="test-file-stub";