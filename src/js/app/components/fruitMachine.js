import React, { Component } from 'react';
import Spinner from './spinner';

import {checkWinner} from '../functions/checkWinner';

class FruitMachine extends Component {
  /**
   * 
   * @param {*} props 
   * set up the three background colors to empty
   */
  constructor(props) {
    super(props);
    this.startApp = this.startApp.bind(this);

    this.state = {
      firstColor: "yellow",
      secondColor: "blue",
      thirdColor: "red"
    }
  }

  /**
   * Start the App by calling the getRandomNumber Method
   */
  startApp(){
    this.getRandomNumbers();
  }

  /**
   * Get three random numbers to set up the colors of the each individual
   * box 
   */
  getRandomNumbers(){
    // Set the initial variables to either zero o empty
    let firstNumber = 0;
    let secondNumber = 0;
    let thirdNumber = 0;
    let isFirtsNumber = false;
    let isSecondNumber = false;
    let isThirdNumber = false;

    // Loop 3 times to store a random number for each box
    for(var i = 0; i < 3; i++){ 
      if(!isFirtsNumber){
        firstNumber = Math.floor(Math.random() * 4 + 0); 
        isFirtsNumber = true;
      }else if(!isSecondNumber){
        secondNumber = Math.floor(Math.random() * 4 + 0);
        isSecondNumber = true;
      }else if(!isThirdNumber){
        thirdNumber = Math.floor(Math.random() * 4 + 0);  
        isThirdNumber = true;
      }//end if
    }//end for loop

    //Call the setColor method and pass the random numbers
    this.setColors(firstNumber, secondNumber, thirdNumber);  
  }

  /**
   * Check if the threee colors are the same. If they are, we have a winner! 
   * @param {*} firstNumber 
   * @param {*} secondNumber 
   * @param {*} thirdNumber 
   */
  checkWinner(firstNumber, secondNumber, thirdNumber){ 
    //call the checkwinner function to test whether the three number are the same.
    let isWinner = checkWinner(firstNumber, secondNumber, thirdNumber);
    if(isWinner){
      alert("WINNER")
    }else{
      //alert("Keep trying");
    }
  }

   /**
    * Set up the background color of each invidual box
    * @param {*} firstNumber 
    * @param {*} secondNumber 
    * @param {*} thirdNumber 
    */ 
  setColors(firstNumber, secondNumber, thirdNumber){
    const colors = ["red", "blue", "green", "yellow"];
    this.setState({
      firstColor: colors[firstNumber],
      secondColor: colors[secondNumber],
      thirdColor: colors[thirdNumber]
    });

    this.checkWinner(firstNumber, secondNumber, thirdNumber);
  }  

  render() {
    return (

        <div className="wrapper">
          <Spinner className="spinner" bgColor={this.state.firstColor}/>
          <Spinner className="spinner" bgColor={this.state.secondColor}/>
          <Spinner className="spinner" bgColor={this.state.thirdColor}/>
          <div className="clear"></div>
          <button className="button" onClick={this.startApp}>PLAY</button>
        </div>  
    )
  }
}

export default FruitMachine;