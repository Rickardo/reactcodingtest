import React, {PropTypes} from 'react';
import IconImage from '../../../../src/assets/images/icon.png';
/**
 * Return a div that represents the box with the random color
 * @param {*} props. Receive the background color 
 */
const Spinner = (props) => {
  return (
    <div className="spinner" style={{backgroundColor:props.bgColor}}>
      <img className="icon" src={IconImage}/>
    </div>
  );
};

Spinner.propTypes = {
    bgColor: PropTypes.string.isRequired
}

export default Spinner;