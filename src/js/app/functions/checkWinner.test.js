import {checkWinner} from './checkWinner';

describe('checkWinner function', () => {

    it('should return true', () => {
        let firstNumner = 2, secondNumber = 2, thirdNumber = 2;
        expect(checkWinner(firstNumner, secondNumber, thirdNumber)).toBe(true);
    });

    it('should return false', () => {
        let firstNumner = 1, secondNumber = 2, thirdNumber = 3;
        expect(checkWinner(firstNumner, secondNumber, thirdNumber)).toBe(false);
    });

});